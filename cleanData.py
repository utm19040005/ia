from pymongo import MongoClient

#Validations 
stops = [
    "UTMA",
    "Terminal Villas, Sector Puertecito",
    "Calle Puerto De Cozumel"
]
routes = [8, 37, 43]

def isValidTime(value):
    separator = value.split("-")
    permithours = ["6", "7", "8", "9", "10", "11", "12", "13",
                   "14", "15", "16", "17", "18", "19", "20", "21"]
    if len(separator) != 2:
        return False
    else:
        condition = separator[1] == str(int(separator[0]) + 1) and separator[0] in permithours and separator[
            1] in permithours
        if condition:
            return True
        else:
            return False


def isValidStop(stop):
    if 0 < int(stop) <= 3:
        postion = int(stop) - 1
        if stops[postion]:
            return True
        else:
            return False
    else:
        return False



#Get database
def getdatabase():
    conn_str = "localhost"
    client = MongoClient(conn_str)
    return client["SmartStops"]

db  = getdatabase();
collection = db['answers']



def rejectRegister(id, reason):
     collection.update_one({
        "_id": id
    },{
        "$set": {
            "useful": False,
            "reason": reason
        }
    })



#Clean DB registers
for document in collection.find({}):
    reason = "";
    if document["stop"] in stops:     
        if isValidTime(document["time"]) == True:           
            if document["route"] in routes:               
                print("Registro válido")                
            else:
                reason = "Ruta no válida"                
                rejectRegister(document["_id"], reason)
        else:
            reason = "Hora no válida"            
            rejectRegister(document["_id"], reason)
    else:
        reason = "Parada no válida"          
        rejectRegister(document["_id"], reason) 

